#include <stdio.h>
#include <pthread.h>
#include <string.h>
#include <sys/time.h>


int aMatrix[100][100] ;
int bMatrix[100][100] ;
int cMatrix[100][100] ;

void readFile(int *n , int *m , char *fileName , int arr[][100] );
void writeToFile(int rowsNum, int colsNum, char *fileName);

void nonThreadedMatMult ();
void threadedMatMultPerRow ();
void threadedMatMultPerElement ();

void *mul_per_element(void *param); /* the thread */
void *mul_per_row(void *param); /* the thread */

int r1,c1;
int r2,c2;

struct mat {
   int row;
   int colum;
};



int main(int argc , char *argv[])
{

      char * aMatrixFile="a.txt" ;
      char * bMatrixFile="b.txt" ;
      char * matOutFile1= "c_1.out" ;
      char * matOutFile2= "c_2.out" ;
      char * matOutFile3= "c_3.out" ;


        readFile(&r1,&c1,aMatrixFile,aMatrix);
        //for(i=0;i<2;i++){for(k=0;k<3;k++){printf("%d \n",aMatrix[i][k]);}}

        readFile(&r2,&c2,bMatrixFile,bMatrix);

        //for(j=0;j<3;j++){for(m=0;m<2;m++){printf("%d \t",bMatrix[j][m]);}}
        //printf("%d \t",r);printf("%d \n",c);
        //printf("%d \t",x);printf("%d \n",z);


printf("\t ************** Mtrix mat program************* \n");
printf("************************************************************************ \n");
         nonThreadedMatMult();
         writeToFile(r1,c2,matOutFile1);
 printf("************************************************************************ \n");
        threadedMatMultPerElement();
        writeToFile(r1,c2,matOutFile2);
printf("************************************************************************ \n");
        threadedMatMultPerRow();
        writeToFile(r1,c2,matOutFile3);



}

//*****************************************************************************

//this is a version of matrix multiplication that does not use threads
void nonThreadedMatMult (){

    struct timeval stop, start;
    gettimeofday(&start, NULL);

   /*******************Code goes here **************************/
   printf("start of nonThreadedMatMult \n");
   int a,b,c;
   int i,k;
   if(c1==r2){
            for(a=0;a<r1;a++){
                for(b=0;b<c2;b++){
                    for(c=0;c<c1;c++){
                        cMatrix[a][b]+=aMatrix[a][c]*bMatrix[c][b];
                    }
                }
            }
        }

    //for(i=0;i<2;i++){printf("\n");for(k=0;k<2;k++){printf("%d \t",cMatrix[i][k]);}}

/**************************************************************************************/
    gettimeofday(&stop, NULL); //end checking time

    printf("\n Seconds taken %lu\n", stop.tv_sec - start.tv_sec);
    printf(" Microseconds taken: %lu\n", stop.tv_usec - start.tv_usec);
}

//******************************************************************************
//this is a version of matrix multiplication that creates a thread to calculate the value of one element of the output matrix

void threadedMatMultPerElement (){


    struct timeval stop, start;
    gettimeofday(&start, NULL);

      /*******************Code goes here **************************/
      printf("start of threadedMatMultPerElement \n");
    int i,j;
   for(i = 0; i < r1; i++) {
      for(j = 0; j < c2; j++) {
         //Assign a row and column for each thread
         struct mat *data = (struct mat *) malloc(sizeof(struct mat));
         data->row = i;
         data->colum = j;
         /* Now create the thread passing it data as a parameter */
         pthread_t tid;       //Thread ID
         pthread_attr_t attr; //Set of thread attributes
         //Get the default attributes
         pthread_attr_init(&attr);
         //Create the thread
         pthread_create(&tid,&attr,mul_per_element,data);
         //Make sure the parent waits for all thread to complete
         pthread_join(tid, NULL);

      }
   }

   //Print out the resulting matrix
   //for(i = 0; i < r1; i++) {printf("\n");for(j = 0; j < c2; j++) {printf("%d ", cMatrix[i][j]);}}



    //for(i=0;i<2;i++){printf("\n");for(k=0;k<2;k++){printf("%d \t",cMatrix[i][k]);}}
    /***************************************************************/
    gettimeofday(&stop, NULL);

    printf("\n Seconds taken %lu\n", stop.tv_sec - start.tv_sec);
    printf("Microseconds taken: %lu\n", stop.tv_usec - start.tv_usec);
}

//******************************************************************************
//this is a version of matrix multiplication that creates a thread to calculate the values of elements in one row of the output matrix
void threadedMatMultPerRow (){

    struct timeval stop, start;
    gettimeofday(&start, NULL);

      /*******************Code goes here **************************/
      printf("start of threadedMatMultPerRow \n");
      int i,j;
   for(i = 0; i < r1; i++) {
      //for(j = 0; j < c2; j++) {
         //Assign a row and column for each thread
         struct mat *data = (struct mat *) malloc(sizeof(struct mat));
         data->row = i;
         //data->j = j;
         /* Now create the thread passing it data as a parameter */
         pthread_t tid;       //Thread ID
        pthread_attr_t attr; //Set of thread attributes
        //Get the default attributes
         pthread_attr_init(&attr);
         //Create the thread
         pthread_create(&tid,&attr,mul_per_row,data);
         //Make sure the parent waits for all thread to complete
         pthread_join(tid, NULL);

      //}
   }

   //Print out the resulting matrix
   //for(i = 0; i < r1; i++) {printf("\n");for(j = 0; j < c2; j++) {printf("%d ", cMatrix[i][j]);}}


    //for(i=0;i<2;i++){printf("\n");for(k=0;k<2;k++){printf("%d \t",cMatrix[i][k]);}}
    /**************************************************************************/
    gettimeofday(&stop, NULL);

    printf("\n Seconds taken %lu\n", stop.tv_sec - start.tv_sec);
    printf("Microseconds taken: %lu\n", stop.tv_usec - start.tv_usec);


}
//******************************************************************************

/******** This function reads a file into a matrix ***********/
// n: will contain number of rows of the read matrix
// m: will contain number of columns of the read matrix
//fileName: name of file to read
// arr: will contain the read matrix
// call the function like:
//int r,c;
//readFile(&r,&c,aMatrixFile,aMatrix);

void readFile(int *n , int *m , char *fileName , int arr[][100] ){
    FILE *in= fopen(fileName, "r") ;
    int nn , mm ;
    char line [1000];
    fgets(line ,1000 , in) ;
    int error = sscanf (line , "row=%i col=%i" ,&nn ,&mm) ;

    if (error==0)
        return ;

    *n=nn ;
    *m=mm;
    int i , j ;
    for (i=0;i<nn;i++){
        for (j=0;j<mm;j++){
            int tmp ;
            fscanf(in , "%d", &tmp) ;
            arr[i][j] = tmp ;
        }
    }
    fclose(in);
}

/******* This function writes the result matrix 'cMatrix' to a file ****/
// rowsNum: number of rows in cMatrix
// colsNum: number of columns in cMatrix
// fileName: name of output file

void writeToFile(int rowsNum, int colsNum, char *fileName){
	FILE *out=fopen(fileName, "w") ;
	   int i,j=0;
	    for (i=0;i<rowsNum;i++){
		for (j=0;j<colsNum;j++){
		    fprintf (out ,"%d\t" ,cMatrix[i][j]);
		}
		fprintf(out ,"\n");
	    }
	    fclose (out);
}

void *mul_per_element(void *param) {
   struct mat *data = param; // the structure that holds our data
   int n, sum = 0; //the counter and sum

   //Row multiplied by column
   for(n = 0; n< c1; n++){
      sum += aMatrix[data->row][n] * bMatrix[n][data->colum];
   }
   //assign the sum to its coordinate
   cMatrix[data->row][data->colum] = sum;

   //Exit the thread
   pthread_exit(0);
}

void *mul_per_row(void *param) {
   struct mat *data = param; // the structure that holds data
   int n,m;
   int sum = 0; //the counter and sum

   //Row multiplied by column
   for(m=0;m<c2;m++){
       sum = 0;
    for(n = 0; n< c1; n++){
      sum += aMatrix[data->row][n] * bMatrix[n][m];
   }
   cMatrix[data->row][m] = sum;
  }

   //Exit the thread
   pthread_exit(0);
}
