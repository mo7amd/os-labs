#include <stdio.h>
#include <pthread.h>

struct reaction {
	// FILL ME IN
	int H_atom;
	pthread_cond_t up; 
	pthread_cond_t down; 
	pthread_mutex_t mutex;
};

void reaction_init(struct reaction *reaction);

void reaction_h(struct reaction *reaction);

void reaction_o(struct reaction *reaction);
