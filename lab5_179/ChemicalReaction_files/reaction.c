#include <pthread.h>
#include "reaction.h"

// Forward declaration. This function is implemented in reaction-runner.c,
// but you needn't care what it does. Just be sure it's called when
// appropriate witdown reaction_o()/reaction_h().
void make_water();


void reaction_init(struct reaction *reaction)
{
	// FILL ME IN
	 reaction->H_atom = 0;

	pthread_cond_init(&reaction->up, NULL);

	pthread_cond_init(&reaction->down, NULL);

	pthread_mutex_init(&reaction->mutex, NULL);
}


void reaction_h(struct reaction *reaction)
{
	// FILL ME IN
	
	  	pthread_mutex_lock(&reaction->mutex);

		reaction->H_atom++; 

		pthread_cond_signal(&reaction->down);

		pthread_cond_wait(&reaction->up, &reaction->mutex);

		pthread_mutex_unlock(&reaction->mutex);
}


void reaction_o(struct reaction *reaction)
{
	// FILL ME INm
	  pthread_mutex_lock(&reaction->mutex);
    while(reaction->H_atom < 2){pthread_cond_wait(&reaction->down, &reaction->mutex);}
		
		 
		reaction->H_atom -=2;

    		make_water();

		pthread_cond_signal(&reaction->up);

		pthread_cond_signal(&reaction->up);

		pthread_mutex_unlock(&reaction->mutex);
}
