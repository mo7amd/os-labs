#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <error.h>
#include <ctype.h>
#include <sys/wait.h>
#include <stdbool.h>

char input_str[512]; // input string from the user
char *buffer_ptr; // temp buffer for store the date
char *data_ptr[10]; // array that make system call

//****************** main function **********************
int main(int argc , char *argv[])
   {
    pid_t pid;
    int i=0;  //counter variable
    int WS_no=0; //counter for white spaces
    int length; //length of input string
    bool background; //flag for backgrund operation
//******************* infinite loop ********************
while(1){

    start: i=0;WS_no=0;background=false;//initial value of variables
    printf("Shell>>");//shell command keep print in the first of every loop
	//execvp("pwd",starter);exit(1);printf("@");
//******************************************************************
    fgets(input_str,512,stdin); //get the command line(fgets add newline)
//**********************************************************************
    // clear the newline at the end of line
    char* p = strchr(input_str,'\n');
    if (p) {*p = '\0';}
//*******************************************************************
	// check for & symbol, empty command and multiple white spaces
	 length=strlen(input_str);
     // printf("%c \n",input_str[length-1]);
	if(input_str[length-1]=='&')
      {input_str[length-1]='\0';background=true;}
      //printf("%c \n",input_str[length-1]);
//*******************************************************************
	 if(length<1){
        //printf("\n");
        goto start;
	}
	else if(length<2){
	printf("f: command not found \n");
	goto start;
	}
	else if(strcmp(input_str,"exit")==0){
	return 0;
	}
//**********************************************************************
	int k=0;
     for(k=0;k<length;k++){
        if(isspace(input_str[k]))
        {
            WS_no++;
        }
        }
//***********************************************************************
//splite first element from the input string and store it in a temp buffer
    buffer_ptr=strtok(input_str," ");
    data_ptr[i]=buffer_ptr; // push data to the array
    // do the same to the rest of the string
    while((buffer_ptr=strtok(NULL," ")) != NULL){
    i++;
   data_ptr[i]=buffer_ptr;
    }

    data_ptr[i+1]=NULL; // add NULL to the end of the array
     //printf("%d \n",WS_no);
     //printf("%d \n",i);
    if(WS_no>i){
        printf("No such file or directory \n");
        goto start;
    }
//*************************************************************
	//check whether the command is cd or otherwise
    //printf("%s \n",data_ptr[0]);
    if(strcmp(data_ptr[0],"cd")==0){
            if((chdir(data_ptr[1])<0)){
			perror("");		
			}
		}
	
    else{
pid = fork();
if(pid==0){ //child process

	// if it is other wise start forking
  //printf("I am herer \n");
 // printf("%s \n",input_str);

              if((execvp(data_ptr[0],data_ptr))<0){
               perror("");
              }
              exit(1);
          
    }

  else { // parent process
              if(!background){ // if it is frontground wait

			int status;
    			waitpid(pid, &status, 0);

                }
            }

    }

    } // while end

} // main end

